import { NavLink } from 'react-router-dom';

function Nav() {

  return (
    <header>
      <nav className="navbar navbar-expand-lg navbar-dark bg-success">
        <div className="container-fluid">
          <NavLink className="navbar-brand" to="/">CarCar</NavLink>
          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li className="nav-item">
                <NavLink className="nav-link" aria-current="page" to="/createautomobile">
                  Create Automobile
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" aria-current="page" to="/createmanufacturer">
                  Create Manufacturer
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" aria-current="page" to="/createvehiclemodel">
                  Create Vehicle Model
                </NavLink>
                </li>
              <li className="nav-item">
                <NavLink className="nav-link" aria-current="page" to="/createappointment">
                  Create New Appointment
                </NavLink>
                </li>
              <li className="nav-item">
                <NavLink className="nav-link" aria-current="page" to="/createtechnician">
                  Add Technician
                </NavLink>
                </li>
              <li className="nav-item">
                <NavLink className="nav-link" aria-current="page" to="/listmanufacturers">
                  List Manufacturers
                </NavLink>
                </li>
              <li className="nav-item">
                <NavLink className="nav-link" aria-current="page" to="/technicians">
                  List Technicians
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" aria-current="page" to="/listinventoryautomobiles">
                  List Inventory Automobiles
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" aria-current="page" to="/listvehiclemodels">
                  List Vehicle Models
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" aria-current="page" to="/listappointments">
                  List Appointments
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" aria-current="page" to="/listappointmenthistory">
                  Appointment History
                </NavLink>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </header>
  )
}

export default Nav;
