import React, { useEffect, useState } from 'react';

function VehicleModels() {
    const [vehicleModels, setVehicleModels] = useState([]);

    const getData = async () => {
        const response = await fetch('http://localhost:8100/api/models/');

        if (response.ok) {
            const data = await response.json();
            console.log(data);
            setVehicleModels(data.models);
        }
    }

    useEffect(()=>{
        getData()
    }, []);

    return (
        <div>
            <h1>Vehicle Models</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Picture Url</th>
                        <th>Manufacturer</th>
                    </tr>
                </thead>
                <tbody>
                    {vehicleModels.map(vehicleModel => {
                        return (
                            <tr key={vehicleModel.name}>
                                <td>{ vehicleModel.picture_url }</td>
                                <td>{ vehicleModel.manufacturer.name }</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}
export default VehicleModels;
